<!DOCTYPE html>
<html>
  <head>
    <title>SysRamais</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" type="text/css" href="{{asset('semantic/dist/semantic.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/painel.css') }}">
  </head>

  <body>
    <div class="container_computer">

      <div class="menu_lateral">
        <div class="superior">
          <p class="title_menu">SysRamais</p>
          <img class="image_menu" src="{{ asset('images/intra_bola_branca.png') }}">
        </div>

        <div class="itens_lateral">

          <p class="item intranet">Intranet</p>

          <p class="item pagina_inicial">Página inicial</p>

          <p class="item abrir_cadastros">Cadastros</p>

          <div class="dropdown_cadastros_content">
            <a href="/setor" class="item_dropdown">Cadastro de Setor</a>
            <a href="/ramal" class="item_dropdown">Cadastro de Ramal</a>
            <a href="/responsavel" class="item_dropdown">Cadastro de Responsável</a>
          </div>
        </div>

        <div class="icons_lateral">
          <p class="icon_lateral">A</p>
          <p class="icon_lateral">B</p>
        </div>
      </div>

      <div class="menu_e_content">
        <div class="menu_superior">
          <i class="bars icon big open_menu"></i>

          <div class="icons_superior">
            <i class="th icon big icon_superior demais_sites"></i>
            <i class="bell icon big icon_superior notifications"></i>
            <i class="user icon big icon_superior perfil"></i>
          </div>
        </div>

        <div class="content">
          @yield('content')
        </div>
      </div>

    </div>

    <!-- VERIFICAÇÃO DO TAMANHO DO DISPOSITIVO NO JAVASCRIPT -->

    <div class="container_mobile">

      <div class="menu_lateral">
        <div class="superior">
          <p class="title_menu">SysRamais</p>
        </div>

        <div class="itens_lateral">

          <p class="item intranet">Intranet</p>

          <p class="item pagina_inicial">Página inicial</p>

          <p class="item abrir_cadastros">Cadastros</p>

          <div class="dropdown_cadastros_content">
            <a href="#" class="item_dropdown">Cadastro de Setor</a>
            <a href="#" class="item_dropdown">Cadastro de Ramal</a>
            <a href="#" class="item_dropdown">Cadastro de Responsável</a>
          </div>
        </div>

        <div class="icons_lateral">
          <p>A</p>
          <p>B</p>
          <p>C</p>
        </div>
      </div>

      <div class="menu_e_content">
        <div class="menu_superior">
          <i class="bars icon big open_menu_mobile"></i>

          <div class="icons_superior">
            <i class="th icon big icon_superior demais_sites"></i>
            <i class="bell icon big icon_superior notifications"></i>
            <i class="user icon big icon_superior perfil"></i>
          </div>
        </div>

        <div class="content">
          @yield('content')
        </div>
      </div>

    </div>

    <script
      src="https://code.jquery.com/jquery-3.1.1.min.js"
      integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
      crossorigin="anonymous"></script>

    <script src="{{ asset('semantic/dist/semantic.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('js/painel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
  </body>
</html>