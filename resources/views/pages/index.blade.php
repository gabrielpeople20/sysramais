@extends('layouts.painel')

<link rel="stylesheet" type="text/css" href="{{ asset('css/pages/index.css') }}">

@section('content')

  <div class="cards">
    <div class="card">
      <div class="information responsaveis">
        <p class="card_value">0</p>
        <p class="card_title">Responsáveis</p>
      </div>

      <div class="lateral l_responsaveis">
        <p class="card_value_lateral">0</p>        
      </div>
    </div>

    <div class="card">
      <div class="information setores">
        <p class="card_value">0</p>
        <p class="card_title">Setores</p>      
      </div>

      <div class="lateral l_setores">
        <p class="card_value_lateral">0</p>        
      </div>
    </div>

    <div class="card">
      <div class="information ramais">
        <p class="card_value">0</p>
        <p class="card_title">Ramais</p>
      </div>

      <div class="lateral l_ramais">
        <p class="card_value_lateral">0</p>        
      </div>
    </div>
  </div>

@endsection

<script src="{{ asset('js/pages/index.js') }}" type="text/javascript"></script>
