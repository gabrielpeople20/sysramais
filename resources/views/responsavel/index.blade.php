@extends('layouts.painel')

<link rel="stylesheet" type="text/css" href="{{ asset('css/pages/index.css') }}">

@section('content')

<button class="ui primary button new_create">New</button>

<div class="ui basic modal">

  <div id="create-bd" >
    <form action="ControllerRamal@store" method="POST" class="ui form" >
      @csrf
                    
      <div class= 'ui segment'> 

        <h1 class="ui dividing header"><i class="icon plus"></i>Adicionar um responsável</h1>

        <div class="field">
          <label>Nome</label>
          <input type="text"  name='nome_responsavel' placeholder="Nome do responsável" required/>
        </div>
                          
        <div class="field">
          <label>Função</label>
          <input type="text" name='funcao_responsavel' placeholder="Função" required/>
        </div>

        <button class="ui primary button" type="submit">Criar</button>
        <p class="ui red button cancel_create">Cancelar</p>
      </div>
                    
    </form>
  </div>

</div>

@endsection

<script src="{{ asset('js/pages/index.js') }}" type="text/javascript"></script>