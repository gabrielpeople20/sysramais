<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ControllerPages@index');

Route::group(['prefix' => 'ramal'], function(){
  Route::get('/', 'ControllerRamal@index');

  Route::get('/novo_ramal', 'ControllerRamal@create');
  Route::post('/', 'ControllerRamal@store');

  Route::get('/apagar/{id}', 'ControllerRamal@destroy');

  Route::get('/editar/{id}', 'ControllerRamal@edit');
  Route::post('/{id}', 'ControllerRamal@update');
});

Route::group(['prefix' => 'responsavel'], function(){
  Route::get('/', 'ControllerResponsavel@index');

  Route::get('/novo_responsavel', 'ControllerResponsavel@create');
  Route::post('/', 'ControllerResponsavel@store');

  Route::get('/apagar/{id}', 'ControllerResponsavel@destroy');

  Route::get('/editar/{id}', 'ControllerResponsavel@edit');
  Route::post('/{id}', 'ControllerResponsavel@update');
});

Route::group(['prefix' => 'setor'], function(){
  Route::get('/', 'ControllerSetor@index');

  Route::get('/novo_setor', 'ControllerSetor@create');
  Route::post('/', 'ControllerSetor@store');

  Route::get('/apagar/{id}', 'ControllerSetor@destroy');

  Route::get('/editar/{id}', 'ControllerSetor@edit');
  Route::post('/{id}', 'ControllerSetor@update');
});