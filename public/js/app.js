$(document).ready(() => {
  tamanho_da_tela = window.screen.width;

  if(tamanho_da_tela > 991) {
    $('.container_computer').addClass('show');
  } else {
    $('.container_mobile').addClass('show');    
  }

  $('.new_create').click (() => {
    $('.ui.basic.modal').modal('show');
  });
  
  $('.cancel_create').click(() => {
    $('.ui.basic.modal').modal('toggle');
  });
});