$(document).ready(()=> {
  $('.abrir_cadastros').on('click', () => {
    $('.dropdown_cadastros_content').toggleClass("show");
  });

  $('.open_menu').on('click', () => {
    $('.menu_lateral').toggleClass("minimizar");

    $('.itens_lateral').toggleClass("hide");
    $('.icons_lateral').toggleClass("show");

    $('.title_menu').toggleClass("hide");
    $('.image_menu').toggleClass("show");
  });

  $('.open_menu_mobile').on('click', () => {
    $('.menu_lateral').toggleClass("hide");
  });

  $('.intranet').on('click', () => {
    window.location.href = "https://ifce.edu.br/maracanau";
  });

  $('.pagina_inicial').on('click', () => {
    window.location.href = "/";
  });
});