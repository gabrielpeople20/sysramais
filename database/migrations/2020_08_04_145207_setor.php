<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Setor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setors', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->text('bloco');

            $table->unsignedBigInteger('ramal_id')->unsigned();
            $table->foreign('ramal_id')->references('id')->on('ramal');

            $table->unsignedBigInteger('responsavel_id')->unsigned();
            $table->foreign('responsavel_id')->references('id')->on('responsavels');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setors');
    }
}
