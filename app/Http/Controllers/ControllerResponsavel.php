<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Responsavel;

class ControllerResponsavel extends Controller
{
    public function index()
    {
        $responsaveis = Responsavel::all();

        return view('responsavel.index', compact('responsaveis'));
    }

    public function create()
    {
        return view('responsavel/novo_responsavel');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'funcao' => 'required'
        ]);

        $responsavel = new Responsavel();
        $responsavel->nome = $request->input('nome_responsavel');
        $responsavel->funcao = $request->input('funcao_responsavel');

        $responsavel->save();

        return redirect('/responsavel');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ramal = Ramais::find($id);
        if (isset($ramal)) {
          return view('ramais/ramais_editar', compact('ramal'));
        }

        return redirect('/ramais');
    }

    public function update(Request $request, $id)
    {
        $ramal = Ramais::find($id);
        if (isset($ramal)) {
          $ramal->numero = $request->input('numeroRamal');
          $ramal->descricao = $request->input('descricaoRamal');
          $ramal->save();

          return redirect('/ramais');
        }

        return redirect('/ramais');
    }

    public function destroy($id)
    {
        $ramal = Ramais::find($id);
        if (isset($ramal)) {
          $ramal->delete();
        }
        return redirect('/ramais');
    }
}
