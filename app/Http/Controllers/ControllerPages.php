<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControllerPages extends Controller
{
    public function index()
    {
      return view('pages.index');
    }
}
