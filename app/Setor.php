<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setor extends Model
{
    protected $table_name = 'setors';

    public function ramais()
    {
        return $this->hasOne(related: Ramal::class, foreignKey: 'ramal_id', localKey: 'id');
    }

    public function responsaveis()
    {
        return $this->hasOne(related: Responsavel::class, foreignKey: 'responsavel_id', localKey: 'id');
    }
}